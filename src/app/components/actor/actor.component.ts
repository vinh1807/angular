import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Actor } from '../../models/actor.model';
import { ActorService } from './actor.service';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent implements OnInit {

  actors: Actor[];

  constructor(private router: Router, private actorService: ActorService) {
  }

  ngOnInit() {
    this.actorService.getAllActors()
      .subscribe(data => {
        this.actors = data;
      });
  }
  deleteActor(actor: Actor) {
    var i = this.actors.indexOf(actor, 0);
    if (i > -1) {
      this.actors.splice(i, 1);
    }
    this.actorService.deleteActor(actor).subscribe();
  }

}
