import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {RequestOptions} from '@angular/http';

import { Actor } from '../../models/actor.model';

let headers = new HttpHeaders();
   headers = headers.append("Authorization", "Basic " + btoa("user1:123456"));
   headers = headers.append("Content-Type", "application/json");
   
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable()
export class ActorService {

  constructor(private http:HttpClient) {
  }

  //private userUrl = 'http://localhost:8080/user-portal/user';
	private actorUrl = 'dvdrental/actor/';

  public getAllActors() {
    return this.http.get<Actor[]>(this.actorUrl+'all', {headers: headers});
  }

  public deleteActor(actor: Actor) {
    return this.http.delete(this.actorUrl + actor.actorId, {headers: headers});
  }

  // public createUser(user) {
  //   return this.http.post<User>(this.userUrl, user);
  // }

}
