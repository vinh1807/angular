import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';

import { Film } from '../../models/film.model';

let headers = new HttpHeaders();
headers = headers.append("Authorization", "Basic " + btoa("user1:123456"));
headers = headers.append("Content-Type", "application/json");


// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type': 'application/json'})
// };

@Injectable()
export class FilmService {

  constructor(private http: HttpClient) {
  }

  private filmUrl = 'dvdrental/film/';

  public getAllFilms() {
    return this.http.get<Film[]>(this.filmUrl + 'all', { headers: headers });
  }

  public getFilmsByPage() {
    return this.http.get<Film[]>(this.filmUrl + '?pageSize=15', { headers: headers });
  }

  public getFilmsByTitle(title: String) {
    return this.http.get<Film[]>(this.filmUrl + 'title/' + title, { headers: headers });
  }

  public deleteFilm(film: Film) {
    return this.http.delete(this.filmUrl + film.filmId, { headers: headers });
  }

  public addFilm(film) {
    return this.http.post<Film>(this.filmUrl, film, { headers: headers });
  }

  public updateFilm(film) {
    console.log(film);
    return this.http.put<Film>(this.filmUrl, film, { headers: headers });
  }

  public getFilmById(id: number) {
    return this.http.get<Film>(this.filmUrl + id, { headers: headers });
  }

}
