import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Language } from '../../models/language.model';
import { Film } from '../../models/film.model';
import { FilmService } from './film.service';

@Component({
	selector: 'app-film-component',
	templateUrl: './film-component.component.html',
	styleUrls: ['./film-component.component.css']
})
export class FilmComponentComponent implements OnInit {

	films: Film[];
	index: number;
	newFilm: Film;
	language: Language;
	currentFilm: Film;
	titleFilter: String;

	constructor(private router: Router, private filmService: FilmService) {
	}

	ngOnInit() {
		this.getAllFilms();
		this.newFilm = new Film();
		this.currentFilm = new Film();
		this.language = new Language();
		this.language.languageId = 1;
		this.language.name = 'English             ';
		this.newFilm.language = this.language;
		this.currentFilm.language = this.language;
	}
	
	validatedFilm(film: Film) {
		if (film.title == null || film.title == '') {
			alert("Please enter title!");
			return false;
		} else if (film.description == null || film.description == '') {
			alert("Please enter description!");
			return false;
		} else if (film.rentalRate == null || film.rentalRate == 0) {
			alert("Please enter rental rate!");
			return false;
		} else if (film.replacementCost == null || film.replacementCost == 0) {
			alert("Please enter replacement cost!");
			return false;
		} else if (film.releaseYear == null || (film.releaseYear < 1901 || film.releaseYear > 2018)) {
			alert("Please enter a valid release year!");
			return false;
		} else return true;
	}

	getFilmsByPage() {
		this.filmService.getFilmsByPage().subscribe(data => {
			this.films = data;
		});
	}

	getAllFilms() {
		this.getFilmsByPage();
		this.filmService.getAllFilms().subscribe(data => {
			this.films = data;
		});
	}

	getFilmsByTitle() {
		this.filmService.getFilmsByTitle(this.titleFilter).subscribe(data => {
			this.films = data;
		});

	}

	deleteFilm(film: Film) {
		var i = this.films.indexOf(film, 0);
		this.filmService.deleteFilm(film).subscribe(data => {
			if (i > -1) {
				this.films.splice(i, 1);
			}
			alert('Film ' + data + ' deleted');
		});
	}

	addFilm(film: Film) {
		if (this.validatedFilm(film)) {
			this.filmService.addFilm(film).subscribe(data => {
				this.films.push(data);
				this.newFilm.description = '';
				this.newFilm.title = '';
				this.newFilm.rentalRate = null;
				this.newFilm.replacementCost = null;
				this.newFilm.releaseYear = null;
				alert('Film added with ID: ' + data.filmId);
			});
		}
	}

	editFilm(film: Film) {
		this.index = this.films.indexOf(film);
		this.currentFilm.filmId = film.filmId;
		this.currentFilm.title = film.title;
		this.currentFilm.description = film.description;
		this.currentFilm.rentalRate = film.rentalRate;
		this.currentFilm.replacementCost = film.replacementCost;
		this.currentFilm.releaseYear = film.releaseYear;
		this.filmService.getFilmById(film.filmId)
			.subscribe(data => {
				this.currentFilm.actors = data.actors;
				this.currentFilm.categories = data.categories;
				this.currentFilm.inventories = data.inventories;
				console.log(this.currentFilm);
			});
	}
	updateFilm(film: Film) {

		if (film.filmId != 0 && this.validatedFilm(film)) {
			this.filmService.updateFilm(film).subscribe(data => {
				film = data;
				if (this.index !== -1) {
					this.films[this.index] = film;
				}
				alert('Film updated!');
			});
		}
	}
}
