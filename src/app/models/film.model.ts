import { Language } from './language.model';
import { Actor } from './actor.model';
import { Inventory } from './inventory.model';
import { Category } from './category.model';

export class Film {

      filmId: number;
      language: Language;
      title: string;
      actors: Actor[];
      categories: Category[];
      inventories: Inventory[];
      description: string;
      rentalRate: number;
      replacementCost: number;
      releaseYear: number;
}