import { Film } from './film.model';

export class Inventory {
  
    inventoryId: number;
    filmId: number;
    storeId: number;
}