import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FilmComponentComponent } from './components/film-component/film-component.component';
import { ActorComponent } from './components/actor/actor.component';


const routes: Routes = [
  { path: 'film', component: FilmComponentComponent },
  { path: 'actor', component: ActorComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
