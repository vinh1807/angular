import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { FilmComponentComponent } from './components/film-component/film-component.component';
import { AppRoutingModule } from './app.routing.module';
import {FilmService} from './components/film-component/film.service';
import {ActorService} from './components/actor/actor.service';
import {HttpClientModule} from "@angular/common/http";
import { ActorComponent } from './components/actor/actor.component';


@NgModule({
  declarations: [
    AppComponent,
    FilmComponentComponent,
    ActorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [FilmService,ActorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
